import Router from '../Router.js';
import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		this.addIngredients();
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<label>
					Image :<br/>
					<input type="text" name="image" placeholder="https://source.unsplash.com/xxxxxxx/600x600">
					<small>Vous pouvez trouver des images de pizza sur <a href="https://unsplash.com/">https://unsplash.com/</a> puis utiliser l'URL <code>https://source.unsplash.com/xxxxxxx/600x600</code> où <code>xxxxxxx</code> est l'id de la photo choisie (celui dans la barre d'adresse)</small>
				</label>
				<label>
					Ingredients :<br/>
					<select id="ingredients" multiple>
					<select>
				</label>
				<label>
					Prix petit format :
					<input type="number" name="price_small" step="0.05">
				</label>
				<label>
					Prix grand format :
					<input type="number" name="price_large" step="0.05">
				</label>

				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = this.element.querySelector('.pizzaForm');
		form.addEventListener('submit', event => {
			event.preventDefault();
			this.submit();
		});
	}

	submit() {
		// D.4. La validation de la saisie

		const form = this.element.querySelector('form'),
			name = form.name.value,
			image = form.image.value,
			price_small = form.price_small.value,
			price_large = form.price_large.value;

		if (name === '' || image === '') {
			alert('Erreur : Tous les champs sont obligatoires');
			return;
		}

		const pizza = {
			name,
			image,
			price_small: Number(price_small),
			price_large: Number(price_large),
		};

		fetch('http://localhost:8080/api/v1/pizzas', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(pizza),
		})
			.then(response => {
				if (!response.status == 201)
					throw new Error(`Erreur :${response.statusText}`);
				alert(`La pizza ${name} a été ajoutée !`);
				Router.navigate('/', true);
			})
			.catch(err => {
				//console.error(err);
				alert(`La pizza n'a pas pu être ajoutée: ${err}`);
			});
	}

	getIngredients() {
		return fetch('http://localhost:8080/api/v1/ingredients')
			.then(response => {
				if (!response.ok) throw new Error(`Erreur :${response.statusText}`);
				return response.json();
			})
			.catch(err => {
				console.error(err);
			});
	}

	addIngredients() {
		let ingredientsHTML = '';
		this.getIngredients()
			.then(resp => {
				resp.forEach(ingredient => {
					ingredientsHTML += `<option>${ingredient.name}</option>`;
				});
			})
			.then(function () {
				console.log(ingredientsHTML);
				document.querySelector('#ingredients').innerHTML = ingredientsHTML;
			});
	}
}
